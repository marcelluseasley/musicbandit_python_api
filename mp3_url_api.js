var mp3monkey = require('mp3monkey');
var mp3zap = require('mp3zap');

var fs = require('fs'),
    http = require('http'),
    thaletas = require('thaletas');

var express = require('express');

var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');



//post artist and song title
app.post('/getsonginfo', function(req, res){
    res.setTimeout(30000, function(){
        console.log("timeout");
        res.send("error-timeout");
    });
    var queryString = req.body.querystring;

    mp3zap(queryString, function (err, tracks) {
            if (!err && tracks.length) {


                tracks = '{"trackss" :' + JSON.stringify(tracks) + '}';
                res.send(JSON.parse(tracks));
            }
            else {
                mp3monkey(queryString,function (err, tracks) {
                        if (!err && tracks.length) {

                            tracks = '{"tracks" :' + JSON.stringify(tracks) + '}';
                            res.send(tracks);
                        } else {
                            return -1;
                        }
                    });

            }
        }
    );



});

function parse(text){

    return text.replace(/[\r\n]/g, "");

}

var server = app.listen(3000, function(){
    var host = server.address().address;
    var port = server.address().port;
    console.log("listening at http://%s:%s", host, port);
});


