#server.py

import musicbandit_api as mba

from flask import Flask
from flask import request
from flask import jsonify
app = Flask(__name__)

@app.route('/', methods=['GET','POST'])
def index():
    message = """
MB_PY_API SERVER<br/>
/artistlist<br/>
artistname:string<br/>
<br/>
/artisttoptracks<br/>
artistid:uri<br/>
<br/>
/artistalbums<br/>
artistid:uri<br/>
<br/>
/albumtracks<br/>
albumid:uri<br/>

"""
    return message

@app.route('/artistlist', methods=['POST'])
def artist_list():
    if request.method == 'POST':
        if request.form['artistname']:
            result = mba.getArtistList(request.form['artistname'])
            if result != {}:
                
                return jsonify(result)
            else:
                return 'no_artist_found'
        else:
            return 'no_artistname_value'


@app.route('/artisttoptracks', methods=['POST'])
def track_list():
    if request.method == 'POST':
        if request.form['artistid']:
            result = mba.getArtistTopTracks(request.form['artistid'])
            if result != {}:
                
                return jsonify(result)
            else:
                return 'no_tracks_found'
        else:
            return 'no_artistid_value'

@app.route('/artistalbums', methods=['POST'])
def album_list():
    if request.method == 'POST':
        if request.form['artistid']:
            result = mba.getArtistAlbums(request.form['artistid'])
            if result != {}:
                
                return jsonify(result)
            else:
                return 'no_albums_found'
        else:
            return 'no_artistid_value'        

@app.route('/albumtracks', methods=['POST'])
def album_track_list():
    if request.method == 'POST':
        if request.form['albumid']:
            result = mba.getAlbumTracks(request.form['albumid'])
            if result != {}:
                
                return jsonify(result)
            else:
                return 'no_albumtracks_found'
        else:
            return 'no_albumid_value'
        

if __name__ == '__main__':    
    app.run("0.0.0.0",port=5000, debug=True)
