import json

#mbrainz_api.py
import musicbrainzngs as mb

import spotipy
spotify = spotipy.Spotify()


#set useragent info; required to get data from API - musicbrainz
mb.set_useragent("testapp","1.0","marcellus.easley@gmail.com")

#spotify
artist_id = None

def getArtistList(artistName):
    topArtists = {}
    artistList = []
    
    res = spotify.search(q="artist:" + artistName, type="artist")
            
    for artist in res['artists']['items']:
        data={}
        if 'name' in artist.keys():
            data['name'] = artist['name']
        if 'uri' in artist.keys():
            data['uri'] = artist['uri']
        if 'followers' in artist.keys() and 'total' in artist['followers'].keys():
            data['followers'] = artist['followers']['total']

        artistList.append(data)
        topArtists['artists'] = artistList
    return topArtists


def getArtistTopTracks(artist_id):
    topTracks = {}
    tracklist = []
    res = spotify.artist_top_tracks(artist_id)

    if len(res['tracks']) > 0:
        for track in res['tracks']:
            data = {}
            if 'name' in track['album'].keys():
                data['album'] = track['album']['name']
            if 'name' in track.keys():
                data['name'] = track['name']
            
            tracklist.append(data)
            topTracks['tracks'] = tracklist
    return topTracks

def getArtistAlbums(artist_id):
    topAlbums = {}
    albumlist = []
    res = spotify.artist_albums(artist_id)

    if len(res['items']) > 0:
        for album in res['items']:
            data = {}
            if 'name' in album.keys():
                data['name'] = album['name']
            if 'uri' in album.keys():
                data['uri'] = album['uri']

            albumlist.append(data)
            topAlbums['albums'] = albumlist
    return topAlbums

def getAlbumTracks(album_id):
    albumTracks = {}
    tracklist = []
    res = spotify.album_tracks(album_id)

    if len(res['items']) > 0:
        for item in res['items']:
            data = {}
            if 'name' in item.keys():
                data['name'] = item['name']
            if 'uri' in item.keys():
                data['uri'] = item['uri']
            if 'track_number' in item.keys():
                data['track_number'] = item['track_number']

            tracklist.append(data)
            albumTracks['tracks'] = tracklist
    return albumTracks
                
                
            
            
            
            
            
        
